var assert = require('assert');
var mongoose = require('mongoose');
let Account = require('../api/models/account-model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

let account = new Account({
    pin: '1234',
    username: 'Andrew',
});

describe('Account', () => {
    beforeEach((done) => {
        account = new Account({
            pin: '1234',
            username: 'Andrew',
        });
        Account.remove({}, (err) => {
            done();
        });
    });
    describe('/GET Accounts', () => {
        it('should GET list of accounts even if database is empty', (done) => {
            chai.request(server)
                .get('/account')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
        it('it should GET an account by a given id', (done) => {
            account.save((err, account) => {
                chai.request(server)
                    .get('/account/' + account.id)
                    .send(account)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('username');
                        res.body.should.have.property('pin');
                        res.body.should.have.property('portfolios');
                        res.body.should.have.property('_id').eql(account.id);
                        done();
                    });
            })
        });
        it('it should send 400 and proper message if during GET account id is improperly formatted', (done) => {
            chai.request(server)
                .get('/account/testing')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Invalid account id! Cannot get account!');
                    done();
                });
        });
        it('it should GET all accounts, 1', (done) => {
            account.save((err, account) => {
                chai.request(server)
                    .get('/account')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            })
        });
        it('it should GET an Account by username', (done) => {
            account.save((err, account) => {
                chai.request(server)
                    .get('/account/username/Andrew')
                    .send(account)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('username');
                        res.body.username.should.be.eql('Andrew');
                        res.body.should.have.property('pin');
                        res.body.pin.should.be.eql('1234');
                        done();
                    });
            })
        });
        it('it should send a 404(not found) error if Account is not found by username', (done) => {
            chai.request(server)
                .get('/account/username/Testing')
                .send(account)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
    describe('/POST Create Account', () => {
        it('it should POST(create) an account with a username of Andrew and a portfolio length of 1', (done) => {
            chai.request(server)
                .post('/account')
                .send({ username: 'Andrew' })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('portfolios');
                    res.body.portfolios.length.should.be.eql(1);
                    res.body.should.have.property('username');
                    res.body.username.should.eql('Andrew')
                    done();
                });
        });
        it('it should POST(create) an account with a pin of 2265', (done) => {
            chai.request(server)
                .post('/account')
                .send({
                    username: 'Andrew',
                    pin: 2265
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('portfolios');
                    res.body.portfolios.length.should.be.eql(1);
                    res.body.should.have.property('username');
                    res.body.username.should.eql('Andrew');
                    res.body.should.have.property('pin');
                    res.body.pin.should.eql('2265')
                    done();
                });
        });
        it('it should attempt to POST with no body but will return a 400', (done) => {
            chai.request(server)
                .post('/account')
                .send({})
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
        it('it should attempt to POST with only a pin but will return a 400', (done) => {
            chai.request(server)
                .post('/account')
                .send({ pin: 2265 })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
        it('it should attempt to POST with a blank username but will return a 400', (done) => {
            chai.request(server)
                .post('/account')
                .send({ username: '' })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
        it('it should attempt to POST(create) an account with a username that already exists but will return a 400', (done) => {
            let newAccount = new Account({
                pin: 2534,
                username: 'Andrew',
            })
            newAccount.save((err, account) => {
                chai.request(server)
                    .post('/account')
                    .send({ username: 'Andrew' })
                    .end((err, res) => {
                        res.should.have.status(400);
                        done();
                    });
            })
        })

    });
    describe('Authentication', () => {
        it('it should authenticate pin successfully', (done) => {
            let account = new Account({
                pin: '8763',
                username: "Testing",
            })
            account.save((err, account) => {
                chai.request(server)
                    .post('/account/verify')
                    .send({
                        pin: '8763',
                        username: "Testing"
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('verified');
                        res.body.verified.should.be.eql(true);
                        done();
                    });
            });
        });
        it('it should fail to authenticate pin if pin does not match', (done) => {
            let account = new Account({
                pin: '8763',
                username: "Testing",
            })
            account.save(() => {
                chai.request(server)
                    .post('/account/verify')
                    .send({
                        pin: '1234',
                        username: 'Testing'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.have.property('verified');
                        res.body.verified.should.be.eql(false);
                        done();
                    });
            });
        });
        it('it should fail to authenticate pin if accound doesn\'t exist', (done) => {
            let account = new Account({
                pin: '8763',
                username: "Testing",
            })
            account.save(() => {
                chai.request(server)
                    .post('/account/verify')
                    .send({
                        pin: '1234',
                        username: 'AccountNotFound'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.have.property('verified');
                        res.body.verified.should.be.eql(false);
                        done();
                    });
            });
        });
    });
});