var assert = require('assert');
var mongoose = require('mongoose');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('Health', () => {
    describe('/GET health', () => {
        it('should return 200 to verify that server is running', (done) => {
            chai.request(server)
                .get('/health')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('It\'s up. Stay humble');
                    done()
                });
        });
        it('should return 503 if fails to connect', (done) => {
            mongoose.connection.close()
            chai.request(server)
                .get('/health')
                .end((err, res) => {
                    res.should.have.status(503);
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Service Unavailable. Please try again later or contact the admin');
                    done();
                })
        })
    })
})