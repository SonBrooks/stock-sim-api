module.exports = {
    "rules": {
        "indent": ["error", 4],
        "comma-dangle": ["error", "never"],
        "prefer-const": ["error"],
        "no-empty": ["error"],
        "no-extra-parens": ["error"],
    },
    "parserOptions": {
        "ecmaVersion": 6,
    }
};