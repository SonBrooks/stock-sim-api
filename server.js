var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    dbURL = process.env.MONGODB_URI || 'mongodb://localhost/stocksimdb',
    mongoose = require('mongoose'),
    Account = require('./api/models/account-model'), //created model loading here
    Holding = require('./api/models/holding-model'),
    Portfolio = require('./api/models/portfolio-model'),
    bodyParser = require('body-parser');

// mongoose instance connection url connection
console.log('Establishing connection to database...')
mongoose.Promise = global.Promise;
mongoose.connect(dbURL);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var health = require('./api/routes/health-routes');
health(app);

var account = require('./api/routes/account-routes'); //importing route
account(app); //register the route

var holding = require('./api/routes/holding-routes');
holding(app);

app.use(function (req, res) {
    res.status(404).send('The page you were looking for can\'t be found: ' + req.originalUrl)
});

console.log('Starting Server...')
app.listen(port);

console.log('Stocksim RESTful API server started on: ' + port);

module.exports = app;