
const mongoose = require('mongoose');

const { Schema } = mongoose;

const AccountSchema = new Schema({
    username: {
        type: String,
        required: 'Please enter the username'
    },
    pin: {
        type: String,
        default: '1234'
    },
    portfolios: {
        type: [mongoose.Schema.Types.Portfolio],
        default: []
    }
});

module.exports = mongoose.model('Accounts', AccountSchema);
