
const mongoose = require('mongoose');

const { Schema } = mongoose;

const PortfolioSchema = new Schema({
    cash: {
        type: Number,
        default: 0.0
    },
    holdings: {
        type: [mongoose.Schema.Types.Holding],
        default: []
    },
    gameId: {
        type: String
    }
});

module.exports = mongoose.model('Portfolio', PortfolioSchema);
