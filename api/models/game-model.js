
const mongoose = require('mongoose');

const { Schema } = mongoose;

const GameSchema = new Schema({
    gameName: {
        type: String,
        default: 'NoName'
    },
    gameId: {
        type: String,
        required: 'Game requires a gameId'
    },
    gamePassword: {
        type: String,
        default: 'password'
    },
    userIds: {
        type: [String],
        default: []
    },
    startingCash: {
        type: Number,
        default: 10000
    },
    commission: {
        type: Number,
        default: 0.0
    },
    startDate: {
        type: Date
    },
    endDate: {
        type: Date
    }
});

module.exports = mongoose.model('Game', GameSchema);
