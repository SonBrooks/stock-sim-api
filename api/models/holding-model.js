
const mongoose = require('mongoose');

const { Schema } = mongoose;

const HoldingSchema = new Schema({
    stockSymbol: {
        type: String,
        required: 'Stock symbol was not specified!'
    },
    quantity: {
        type: Number,
        default: 0
    },
    purchasePrice: {
        type: Number,
        default: 0.0
    }
});

module.exports = mongoose.model('Holding', HoldingSchema);
