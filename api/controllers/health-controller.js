
const mongoose = require('mongoose');

exports.isUp = function isUp(req, res) {
    if (mongoose.connection.readyState === 1) {
        res.status(200).send({ message: 'It\'s up. Stay humble' });
    } else {
        res.status(503).send({ message: 'Service Unavailable. Please try again later or contact the admin' });
    }
};
