const mongoose = require('mongoose');

const Holding = mongoose.model('Holding');
const Accounts = mongoose.model('Accounts');

const axios = require('axios');

const service = {
    getPrice: symbol => axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/price`)
};

function addNewHoldingToPortfolio(accountId, newHolding, res) {
    Accounts.findByIdAndUpdate(
        accountId,
        {
            $push: { 'portfolios.0.holdings': newHolding }
        },
        { new: true },
        (err, account) => {
            if (err) {
                res.status(500).send(err);
            } else if (account) {
                res.status(200).send(account);
            } else {
                res.status(400).send('Holdings were not added because account not found.');
            }
        }
    );
}

function buyHolding(req, res) {
    const { symbol, quantity } = req.body;
    const { accountId } = req.params;
    const price = service.getPrice(symbol);
    const newHolding = new Holding({ stockSymbol: symbol, quantity, purchasePrice: price.data });

    Accounts.findById(accountId, (err, account) => {
        let addNewHolding = true;

        if (account) {
            const currentHoldings = account.portfolios[0].holdings;
            let updateAccount = false;
            for (let i = 0; i < currentHoldings.length; i += 1) {
                if (currentHoldings[i].stockSymbol === symbol) {
                    updateAccount = true;
                    addNewHolding = false;
                    currentHoldings[i].quantity += Number(quantity);
                    break;
                }
            }
            if (updateAccount) {
                Accounts.findOneAndUpdate(
                    accountId,
                    { $set: { 'portfolios.0.holdings': currentHoldings } },
                    { new: true },
                    (error, accountUpdated) => {
                        if (error) {
                            res.status(500).send(`Failed to update document ${error}`);
                        } else if (accountUpdated) {
                            res.status(200).send(accountUpdated);
                        }
                    }
                );
            }
            if (addNewHolding) {
                addNewHoldingToPortfolio(accountId, newHolding, res);
            }
        } else {
            res.status(400).send('Account not found! Holdings not added.');
        }
    });
}

function sellHolding(res) {
    res.send('Function not implemented');
}

exports.makeOrder = function makeOrder(req, res) {
    if (req.body.orderType === '1') {
        buyHolding(req, res);
    } else if (req.body.orderType === '0') {
        sellHolding(req, res);
    } else {
        res.send('ERROR: INVALID ORDER TYPE.');
    }
};

exports.listHoldings = function listHoldings(req, res) {
    const { accountId } = req.params;
    Accounts.findById(accountId, (err, account) => {
        if (err) {
            res.send(err);
        }
        if (account) {
            res.json(account.portfolios[0].holdings);
        } else {
            res.send('Holdings not found for account.');
        }
    });
};
