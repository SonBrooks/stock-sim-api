

const mongoose = require('mongoose');
const chalk = require('chalk');
const Account = mongoose.model('Accounts');
const Portfolio = mongoose.model('Portfolio');
const log = console.log;


exports.listAllAccounts = function listAllAccounts(req, res) {
    console.log('account-controller.js >>> listAllAccounts called...')
    Account.find({}, (err, accounts) => {
        if (err) {
            log(chalk.red('account-controller.js >>> listAllAccounts ERROR', err))
            res.status(400).send(err);
        } else {
            log('account-controller.js >>> listAllAccounts accounts found successfully!\n', accounts);
            res.status(200).send(accounts);
        }
    });
};

exports.getAccountFromUserName = function getAccountFromUserName(req, res) {
    Account.findOne({ username: req.params.username }, (err, account) => {
        if (err) {
            log(chalk.red('account-controller.js >>> getAccountFromUserName ERROR', err));
            res.status(500).send({ message: 'Internal Server Error >>> Failed to get account from username' })
        }
        if (account) {
            log('account-controller.js >>> getAccountFromUserName Account found!', account)
            res.status(200).send(account);
        } else {
            log('account-controller.js >>> getAccountFromUserName account not found!');
            res.status(404).send({ message: 'Account not found for username.' });
        }
    });
};

exports.createAccount = function createAccount(req, res) {
    Account.findOne({ username: req.body.username }, (error, account) => {
        if (error) {
            log(chalk.red('account-controller.js >>> createAccount error', error));
            res.status(500).send({ message: 'Internal server error.', error: error });
        }
        if (!account) {
            const newAccount = new Account(req.body);
            const newPortfolio = new Portfolio({ cash: 10000 });
            newAccount.portfolios.push(newPortfolio);
            newAccount.save((err, accountSaved) => {
                if (err) {
                    log(chalk.red('account-controller.js >>> createAccount ERROR. Account failed to save!', err))
                    res.status(400).send(`ERROR: ${err}`);
                } else {
                    log('account-controller.js >>> createAccount Account saved!', accountSaved);
                    res.status(201).json(accountSaved);
                }
            });
        } else {
            log('account-controller.js >>> createAccount, account did not save because username already existed', req.body.username);
            res.status(400).send(`Username already exists! ${req.body.username}`);
        }
    });
};

exports.getAccount = function getAccount(req, res) {
    if (req.params.accountId.match(/^[0-9a-fA-F]{24}$/)) {
        Account.findById(req.params.accountId, (err, account) => {
            if (err) {
                res.status(400).send(err);
            } else if (!account) {
                res.status(400).send({ message: 'Could not find an account!', error: err });
            } else {
                res.status(200).json(account);
            }
        });
    } else {
        res.status(400).send({ message: 'Invalid account id! Cannot get account!' });
    }
};

exports.verifyPinForAccount = function verifyPinForAccount(req, res) {
    Account.findOne({ username: req.body.username }, (err, account) => {
        if (!account || err) {
            res.status(400).send({ message: 'Account not found!', verified: false });
        } else if (req.body.pin.toString() === account.pin.toString()) {
            res.status(200).send({ verified: true });
        } else {
            res.status(400).send({ message: 'Pin incorrect. Access denied.', verified: false });
        }
    });
};

exports.updateAccount = function updateAccount(req, res) {
    Account.findOneAndUpdate(
        { _id: req.params.accountId },
        req.body, { new: true },
        (err, account) => {
            if (err) {
                res.status(400).send(err);
            } else {
                res.status(201).json(account);
            }
        }
    );
};

exports.deleteAccount = function deleteAccount(req, res) {
    Account.remove({
        _id: req.params.accountId
    }, (err) => {
        if (err) {
            res.status(400).send(err);
        }
        else {
            res.json({ message: 'Account successfully deleted' });
        }
    });
};
