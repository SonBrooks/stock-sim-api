const mongoose = require('mongoose');

const Portfolio = mongoose.model('Portfolio');

exports.listAllPortfolios = function listAllPortfolios(res) {
    Portfolio.find({}, (err, portfolio) => {
        if (err) res.send(err);
        res.json(portfolio);
    });
};

exports.createPortfolio = function createPortfolio(req, res) {
    const newPortfolio = new Portfolio(req.body);
    newPortfolio.cash = 10000.0;
    newPortfolio.save((err, account) => {
        if (err) {
            res.send(err);
        } else if (account) {
            res.status(200).json(account);
        } else {
            res.status(400).send('Account not found. Portfolio not created.');
        }
    });
};

exports.getPortfolio = function getPortfolio(req, res) {
    Portfolio.findById(req.params.portfolioId, (err, portfolio) => {
        if (err) res.send(err);
        res.json(portfolio);
    });
};

exports.updatePortfolio = function updatePortfolio(req, res) {
    Portfolio.findOneAndUpdate({
        _id: req.params.portfolioId
    }, req.body, {
        new: true
    }, (err, portfolio) => {
        if (err) res.send(err);
        res.json(portfolio);
    });
};

exports.deletePortfolio = function deletePortfolio(req, res) {
    Portfolio.remove({
        _id: req.params.portfolioId
    }, (err, account) => {
        if (err) {
            res.send(err);
        } else if (account) {
            res.status(200).send('Portfolio successfully deleted');
        } else {
            res.status(400).send('Account not found. Portfolio not deleted.');
        }
    });
};
