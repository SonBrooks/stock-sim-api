const accounts = require('../controllers/account-controller');

module.exports = function accountRoutes(app) {
    // Account Routes
    app.route('/account')
        .get(accounts.listAllAccounts)
        .post(accounts.createAccount);

    app.route('/account/:accountId')
        .get(accounts.getAccount)
        .put(accounts.updateAccount)
        .delete(accounts.deleteAccount);

    app.route('/account/username/:username')
        .get(accounts.getAccountFromUserName);

    app.route('/account/verify')
        .post(accounts.verifyPinForAccount);
};
