const holding = require('../controllers/holding-controller');

module.exports = function holdingRoutes(app) {
    // Holding Routes
    app.route('/holding');

    app.route('/holding/:accountId')
        .get(holding.listHoldings)
        .put(holding.makeOrder);
};
