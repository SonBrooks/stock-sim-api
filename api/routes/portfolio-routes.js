const portfolios = require('../controllers/portfolio-controller');

module.exports = function portfolioRoutes(app) {
    // Account Routes
    app.route('/portfolio')
        .get(portfolios.listAllPortfolios);

    app.route('/portfolio/:portfolioId')
        .get(portfolios.getPortfolio)
        .put(portfolios.updatePortfolio)
        .delete(portfolios.deletePortfolio);
};
