const health = require('../controllers/health-controller');

module.exports = function healthRoutes(app) {
    app.route('/health')
        .get(health.isUp);
};
